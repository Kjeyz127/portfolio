import React from 'react';
import '../App.css';

export default function Skill() {
  return (
    <div className="skills">
    <ol className="list">
      <li className="item">
        <h2>Front-End</h2>
        <span>
          ReactJs, HTML, CSS, React Native, Bootstrap, TailwindCSS and Git
        </span>
      </li>
      <li className="item">
        <h2>Back-End</h2>
        <span>
        JavaScript, MongoDBB, Node.js, Postman, Express.js and Heroku
         </span>
      </li>
      
    </ol>
    </div>
    )
}

