import React from "react";
import './Footer.css';
import { Link } from 'react-router-dom';


function Footer() {
  return (

    <div className="footer">
    	<div className="icon">
    		<i class='fab fa-facebook-f' />
    		<i class='fab fa-twitter' />
      	 	<i class='fab fa-linkedin' />
     		<i class='fab fa-instagram' />
    	 </div>
      <p> &copy; 2023 kjamestech.com</p>
    </div>
  );
}

export default Footer;