import React from 'react';
import '../../App.css';
import Skill from '../Skill';
import Footer from '../Footer';

export default function Skills() {
  return (
     <>
      <Skill />
      <Footer />
     </>
    );
}

