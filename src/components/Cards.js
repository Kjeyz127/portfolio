import React from 'react';
import './Cards.css';
import CardItem from './CardItem';

function Cards() {
  return (
    <div className='cards'>
      <h1>Projects</h1>
      <div className='cards__container'>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
            <CardItem
              src='images/kjames.jpeg'
              text='HTML Responsive Web Design'
              label='HTML'
              path='/blank'
            />
            <CardItem
              src='images/api.jpeg'
              text='REST API routes'
              label='API'
              path='/blank'
            />
          </ul>
          <ul className='cards__items'>
             <CardItem
              src='images/img-1.jpeg'
              text='Blank'
              label='soon'
              path='/blank'
            />
            <CardItem
              src='images/ecom.jpeg'
              text='Responsive e-commerce web design with admin and user interface'
              label='e-commerce'
              path='/blank'
            />
            <CardItem
              src='images/img-2.jpeg'
              text='blank'
              label='soon'
              path='/blank'
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;