import React from 'react';
import '../App.css';
import { Button } from './Button';
import './HeroSection.css';

function HeroSection() {
  return (
    <div className='hero-container'>
      <video src='/videos/video-2.mp4' autoPlay loop muted />
      <h1>Hi, My name is Kevin James</h1>
      <p>Aspiring to be a web developer with a passion of learning and creating.</p>
      <div className='hero-btns'>
        <Button
          className='btns'
          buttonStyle='btn--outline'
          buttonSize='btn--large'
          onClick={console.log('hey')}
        >
          View Projects <i className='far fa-play-circle' />
        </Button>
      </div>
    </div>
  );
}

export default HeroSection;